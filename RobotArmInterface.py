import logging
import usb.core
import usb.util
import time
import copy
import signal
import sys

# setup logging if necessary
if __name__ == "__main__":
	logging.basicConfig(format="%(asctime)-15s %(levelname)s [%(thread)d] %(module)s %(message)s", level=logging.DEBUG)

"Interfaces with the USB robotic arm"
class RobotArmInterface:
	# USB details
	ARM_VENDOR_ID       = 0x1267
	ARM_PRODUCT_ID      = 0x0
	ARM_SEARCH_INTERVAL = 10

	# Byte 2: LED power
	LED_BYTE          = 2
	LED_ON            = 0b00000001 # OR
	LED_OFF           = 0b00000000 # AND

	# Byte 1: Base rotation
	BASE_BYTE         = 1
	BASE_RIGHT        = 0b00000001 # OR
	BASE_LEFT         = 0b00000010 # OR
	BASE_OFF          = 0b00000000 # AND

	# Byte 0: Grip and arm
	GRIP_CLOSE        = 0b00000001 # OR
	GRIP_OPEN         = 0b00000010 # OR
	GRIP_OFF          = 0b11111100 # AND

	JOINT_UPPER_UP    = 0b00000100 # OR
	JOINT_UPPER_DOWN  = 0b00001000 # OR
	JOINT_UPPER_OFF   = 0b11110011 # AND
	
	JOINT_MIDDLE_UP   = 0b00010000 # OR
	JOINT_MIDDLE_DOWN = 0b00100000 # OR
	JOINT_MIDDLE_OFF  = 0b11001111 # AND

	JOINT_LOWER_UP    = 0b01000000 # OR
	JOINT_LOWER_DOWN  = 0b10000000 # OR
	JOINT_LOWER_OFF   = 0b00111111 # AND

	ARM_BYTE          = 0
	ARM_OFF           = 0b00000000 # AND

	OFF               = [0, 0, 0]

	"Searches for this arm on start up"
	def __init__(self):
		# default
		self.log	= logging.getLogger("RobotArmInterface")
		self.device     = None
		self.state      = copy.deepcopy(RobotArmInterface.OFF)
		self.stateDirty = True
		
		self.searchForArm()
		self._hookToShutdown()
	
	"Hook for a clean shutdown"
	def _hookToShutdown(self):
		hooked = []
		unhooked = []
		for i in [x for x in dir(signal) if x.startswith("SIG")]:
			try:
				signum = getattr(signal, i)
				signal.signal(signum, self.stop)
				hooked.append(str(signum) + ":" + i);
			except:
				exception = sys.exc_info()[0]
				unhooked.append(str(signum) + ":" + i)
		
		self.log.debug("Signals hooked=%s unhooked=%s", hooked, unhooked)

	"Continously looks for the arm"
	def searchForArm(self):
		self.log.info("Searching for arm (0x%x, 0x%x)", RobotArmInterface.ARM_VENDOR_ID, RobotArmInterface.ARM_PRODUCT_ID)

		while (self.device is None):
			self.device = self._findDevice()

			if (self.device is None):
				self.log.warning("Arm was not found, please ensure it is plugged in. Re-checking in %ds", RobotArmInterface.ARM_SEARCH_INTERVAL)
				time.sleep(RobotArmInterface.ARM_SEARCH_INTERVAL)
			else:		
				self.log.debug("Successfully located %s, resetting state", self.device)
				self.stop()

	"Tries to find the USB device"
	def _findDevice(self):
		return usb.core.find(idVendor=RobotArmInterface.ARM_VENDOR_ID, idProduct=RobotArmInterface.ARM_PRODUCT_ID)

	"Stop the arm completely"
	def stop(self, signal = None, frame = None):
		isSignal = signal is not None and frame is not None

		if (isSignal):
			self.log.info("Received stop signal %s %s", signal, frame)

		self.updateState(RobotArmInterface.ARM_OFF, RobotArmInterface.ARM_BYTE)
		self.updateState(RobotArmInterface.BASE_OFF, RobotArmInterface.BASE_BYTE)
		self.updateState(RobotArmInterface.LED_OFF, RobotArmInterface.LED_BYTE)
		self.flushState()

		if (isSignal):
			self.log.info("Exiting now")
			exit(0)

	"Update internal state"
	def updateState(self, newByte, byteIndex):
		oldByte = self.state[byteIndex]
		isOff = self._isOffByte(newByte, byteIndex)

		self.state[byteIndex] = oldByte & newByte if isOff else oldByte | newByte
		self.stateDirty = self.stateDirty if oldByte == newByte and self.device is not None else True 

	"See if the given control byte is a power off"
	def _isOffByte(self, byte, index):
		if index == RobotArmInterface.ARM_BYTE:
			return RobotArmInterface.ARM_OFF == byte
		elif index == RobotArmInterface.BASE_BYTE:
			return RobotArmInterface.BASE_OFF == byte or RobotArmInterface.GRIP_OFF == byte or RobotArmInterface.JOINT_UPPER_OFF == byte or RobotArmInterface.JOINT_MIDDLE_OFF == byte or RobotArmInterface.JOINT_LOWER_OFF == byte
		elif index == RobotArmInterface.LED_BYTE:
			return RobotArmInterface.LED_OFF == byte
		else:
			self.log.error("Unhandled byte index %d", index)
			return False

	"Writes internal state to arm, return True is successful"
	def flushState(self):
		try:
			if (self.stateDirty):
				self.log.debug("Flushing state (%s, %s, %s)", bin(self.state[RobotArmInterface.ARM_BYTE]), bin(self.state[RobotArmInterface.BASE_BYTE]), bin(self.state[RobotArmInterface.LED_BYTE]))
				self.device.ctrl_transfer(0x40, 6, 0x100, 0, self.state, 500)
				self.stateDirty = False
			return True
		except:
			exception = sys.exc_info()[0]
			self.log.error("Problem flushing %s state %s", "dirty" if self.stateDirty else "clean", exception)

			self.device = None
			return False

if __name__ == "__main__":
	rai = RobotArmInterface()

	i = 1
	while True:
		rai.log.info("Start cycle %d", i)
		rai.updateState(RobotArmInterface.LED_ON, RobotArmInterface.LED_BYTE)
		rai.updateState(RobotArmInterface.JOINT_UPPER_UP, RobotArmInterface.ARM_BYTE)
		rai.updateState(RobotArmInterface.JOINT_LOWER_UP, RobotArmInterface.ARM_BYTE)
		rai.updateState(RobotArmInterface.BASE_RIGHT, RobotArmInterface.BASE_BYTE)
		rai.flushState()
		time.sleep(2)
		
		rai.updateState(RobotArmInterface.ARM_OFF, RobotArmInterface.ARM_BYTE)
		rai.updateState(RobotArmInterface.BASE_OFF, RobotArmInterface.BASE_BYTE)
		rai.updateState(RobotArmInterface.JOINT_UPPER_DOWN, RobotArmInterface.ARM_BYTE)
		rai.updateState(RobotArmInterface.JOINT_LOWER_DOWN, RobotArmInterface.ARM_BYTE)
		rai.updateState(RobotArmInterface.BASE_LEFT, RobotArmInterface.BASE_BYTE)
		rai.flushState()
		time.sleep(2)
		
		rai.stop()
		rai.log.info("Completed cycle %d", i)
		i += 1
		time.sleep(2)

# README #

A small USB Python driver intended for use on the Raspberry Pi to interface with the [OWI black 'n yellow USB Robotic Arm](http://www.maplin.co.uk/p/robotic-arm-kit-with-usb-pc-interface-a37jn).

This driver allows simultaneous actions by letter the caller pre-define the bits that needs to be enable before flushing to the device. Conversely, clearing all bits will halt it. Please refer to "main" for and example usage

You can run the demo instruction set via:

```
#!bash

python RobotArmInterface.py
```
